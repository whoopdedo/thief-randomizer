/******************************************************************************
 *  Allocator.cpp
 *
 *  This file is part of Thief Randomizer
 *  Copyright (C) 2022 Tom N Harris <telliamed@whoopdedo.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/
#include "globals.h"
#include <new>
#include <cassert>
#include <cstring>

void* operator new(std::size_t size)
{
	void* ptr = g_pMalloc->Alloc(size);
	if (!ptr)
		throw std::bad_alloc();
	return ptr;
}

void* operator new(std::size_t size, std::align_val_t)
{
	void* ptr = g_pMalloc->Alloc(size);
	if (!ptr)
		throw std::bad_alloc();
	return ptr;
}

void* operator new[](std::size_t size)
{
	void* ptr = g_pMalloc->Alloc(size);
	if (!ptr)
		throw std::bad_alloc();
	return ptr;
}

void* operator new[](std::size_t size, std::align_val_t)
{
	void* ptr = g_pMalloc->Alloc(size);
	if (!ptr)
		throw std::bad_alloc();
	return ptr;
}

void operator delete(void* ptr)
{
	if (ptr)
		g_pMalloc->Free(ptr);
}

void operator delete(void* ptr, std::size_t)
{
	if (ptr)
		g_pMalloc->Free(ptr);
}

void operator delete(void* ptr, std::align_val_t)
{
	if (ptr)
		g_pMalloc->Free(ptr);
}

void operator delete(void* ptr, std::size_t, std::align_val_t)
{
	if (ptr)
		g_pMalloc->Free(ptr);
}

void operator delete[](void* ptr)
{
	if (ptr)
		g_pMalloc->Free(ptr);
}

void operator delete[](void* ptr, std::size_t)
{
	if (ptr)
		g_pMalloc->Free(ptr);
}

void operator delete[](void* ptr, std::align_val_t)
{
	if (ptr)
		g_pMalloc->Free(ptr);
}

void operator delete[](void* ptr, std::size_t, std::align_val_t)
{
	if (ptr)
		g_pMalloc->Free(ptr);
}

