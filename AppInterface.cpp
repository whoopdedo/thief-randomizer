/******************************************************************************
 *  AppInterface.cpp
 *
 *  This file is part of Thief Randomizer
 *  Copyright (C) 2022 Tom N Harris <telliamed@whoopdedo.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/
#include "globals.h"
#include <lg/scrservices.h>

typedef TInterfaceLinkedList<IScriptMan> InterfaceLinkedListHead;

void AppIface_::MakeAppInterface(IID const& _riid, InterfaceLinkedList* _p)
{
	IUnknown* pTemp;
	auto _sm = AppInterface<IScriptMan>::pointer;
	if (E_NOINTERFACE != _sm.iface->QueryInterface(_riid, (void**)&pTemp))
	{
		_p->iface = pTemp;
		_p->next = (InterfaceLinkedList*)_sm.next;
		_sm.next = (InterfaceLinkedListHead*)_p;
	}
}

void AppIface_::MakeAppService(IID const& _riid, InterfaceLinkedList* _p)
{
	auto _sm = AppInterface<IScriptMan>::pointer;
	if (!null_service.iface)
	{
		null_service.iface = _sm.iface->GetService(IID_INullScriptService);
		null_service.next = (InterfaceLinkedList*)_sm.next;
		_sm.next = (InterfaceLinkedListHead*)&null_service;
	}
	IUnknown* pTemp = _sm.iface->GetService(_riid);
	if (pTemp == null_service.iface)
	{
		pTemp->Release();
	}
	else
	{
		_p->iface = pTemp;
		_p->next = (InterfaceLinkedList*)_sm.next;
		_sm.next = (InterfaceLinkedListHead*)_p;
	}
}
