/******************************************************************************
 *  AppInterface.h
 *
 *  This file is part of Thief Randomizer
 *  Copyright (C) 2022 Tom N Harris <telliamed@whoopdedo.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

template<typename _IFace>
struct TInterfaceLinkedList
{
	_IFace* iface;
	TInterfaceLinkedList* next;
};

typedef TInterfaceLinkedList<IUnknown> InterfaceLinkedList;

class AppIface_
{
	AppIface() = delete;
protected:
	static void MakeAppInterface(IID const& _riid, InterfaceLinkedList* _p);
	static void MakeAppService(IID const& _riid, InterfaceLinkedList* _p);
	static inline InterfaceLinkedList null_service = {nullptr, nullptr};
};

template<class _IFace, typename _IID = IID_Def<_IFace> >
class AppInterface : AppIface_
{
public:
	AppInterface()
	{
		if (!pointer.iface)
			MakeAppInterface(_IID::iid(), (InterfaceLinkedList*)&pointer);
	}
	_IFace* operator-> () const { return (_IFace*)pointer.iface; }
protected:
	static inline TInterfaceLinkedList<_IFace> pointer = {nullptr, nullptr};
};

template<class _IFace, typename _IID = IID_Def<_IFace> >
class AppService : AppIface_
{
public:
	AppService()
	{
		if (!pointer.iface)
			MakeAppService(_IID::iid(), (InterfaceLinkedList*)&pointer);
	}
	_IFace* operator-> () const { return (_IFace*)pointer.iface; }
protected:
	static inline TInterfaceLinkedList<_IFace> pointer = {nullptr, nullptr};
};

template<>
class AppInterface<IScriptMan, IID_Def<IScriptMan> >
{
public:
	AppInterface() { }
	static void Init(IScriptMan* _sm)
	{
		AppInterface<IScriptMan>::pointer.iface = _sm;
	}
	static void Release()
	{
		auto _p = (InterfaceLinkedList*)AppInterface<IScriptMan>::pointer.next;
		AppInterface<IScriptMan>::pointer.next = nullptr;
		while (_p)
		{
			_p->iface->Release();
			_p->iface = nullptr;
			auto _n = _p->next;
			_p->next = nullptr;
			_p = _n;
		}
	}
protected:
	friend class AppIface_;
	static inline TInterfaceLinkedList<IScriptMan> pointer = {nullptr, nullptr};
};
