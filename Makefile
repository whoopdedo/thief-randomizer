###############################################################################
##  Makefile-gcc
##
##  This file is part of Thief Randomizer
##  Copyright (C) 2022 Tom N Harris <telliamed@whoopdedo.org>
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.
##
###############################################################################


.SUFFIXES:
.SUFFIXES: .o .cpp .rc
.PRECIOUS: %.o
.PHONY: liblg

GAME = 2

srcdir = .
bindir = bin

LGDIR = lg

PLAT =
CC = $(PLAT)gcc
CXX = $(PLAT)g++
AR = $(PLAT)ar
LD = $(PLAT)g++
DLLTOOL = $(PLAT)dlltool
RC = $(PLAT)windres
MKDIR = mkdir
RM = rm

DEFINES = -DWINVER=0x0400 -D_WIN32_WINNT=0x0400 -DWIN32_LEAN_AND_MEAN
GAMEDEF = -D_DARKGAME=$(GAME) -D_NEWDARK -DPLAYTEST

ifdef DEBUG
DEFINES := $(DEFINES) -DDEBUG
CXXDEBUG = -g -O0
LDDEBUG = -g
LGLIB = $(bindir)/liblg-d.a
else
DEFINES := $(DEFINES) -DNDEBUG
CXXDEBUG = -O2
LDDEBUG =
LGLIB = $(bindir)/liblg.a
endif

ARFLAGS = rc
LDFLAGS = -mwindows -mdll -static-libgcc -static-libstdc++ -Wl,--enable-auto-image-base
LIBDIRS = -L. -L$(bindir)
LIBS = -luuid
INCLUDES = -I. -I$(srcdir) -I$(LGDIR)
# If you care for this... # -Wno-unused-variable
# A lot of the callbacks have unused parameters, so I turn that off.
CXXFLAGS = -W -Wall -Wno-unused-parameter -masm=intel
DLLFLAGS = --add-underscore

OSM_OBJS = $(bindir)/exports.o


$(bindir)/%.o: $(srcdir)/%.cpp
	$(CXX) $(CXXFLAGS) $(CXXDEBUG) $(DEFINES) $(GAMEDEF) $(INCLUDES) -o $@ -c $<

$(bindir)/%_res.o: $(srcdir)/%.rc
	$(RC) $(DEFINES) $(GAMEDEF) -o $@ -i $<

%.o: %.cpp
	$(CXX) $(CXXFLAGS) $(CXXDEBUG) $(DEFINES) $(GAMEDEF) $(INCLUDES) -o $@ -c $<

%_res.o: %.rc
	$(RC) $(DEFINES) -o $@ -i $<

%.osm: %.o $(OSM_OBJS)
	$(LD) $(LDFLAGS) $(LDDEBUG) $(LIBDIRS) -o $@ script.def $< $(OSM_OBJS) $(LIBS)

ALL: random.osm

clean:
	$(RM) $(bindir)/*.a
	$(RM) $(bindir)/*.o
	$(RM) $(bindir)/lg/*.o

liblg: | $(bindir)/lg
	$(MAKE) -C $(LGDIR) bindir=$(realpath $(bindir)/lg) libdir=$(realpath $(bindir)) PLAT=$(PLAT)

OSM_SRCS = globals.cpp \
	Allocator.cpp \
	AppInterface.cpp \
	Monolog.cpp \
	SingleInstance.cpp \
	SingleScript.cpp \
	SingleScriptModule.cpp \
	Randomizer.cpp \
	Selectors.cpp \
	ThiefRandomizer.cpp \
	ThiefSelectors.cpp \
	ThiefReplacers.cpp \
	AIRandomizer.cpp \
	PickupRandomizer.cpp
OSM_OBJS = $(OSM_SRCS:%.cpp=$(bindir)/%.o) $(bindir)/exports.o

#.INTERMEDIATE: $(bindir)/exports.o

random.osm: liblg $(OSM_OBJS)
	$(LD) $(LDFLAGS) $(LDDEBUG) -o $@ $(OSM_OBJS) $(LGLIB) $(LIBS)

$(bindir)/exports.o: $(bindir)/SingleScriptModule.o | $(bindir)
	$(DLLTOOL) $(DLLFLAGS) --dllname random.osm --output-exp $@ $^

$(bindir): ; $(MKDIR) $@

$(bindir)/lg: | $(bindir) ; $(MKDIR) $@

$(bindir)/AIRandomizer: AIRandomizer.cpp ThiefRandomizer.h Generator.h Monolog.h AppInterface.h globals.h | $(LGLIB) $(bindir)
$(bindir)/PickupRandomizer: PickupRandomizer.cpp ThiefRandomizer.h Generator.h Monolog.h AppInterface.h globals.h | $(LGLIB) $(bindir)
$(bindir)/ThiefReplacers.o: ThiefReplacers.cpp ThiefReplacers.h Monolog.h AppInterface.h globals.h | $(LGLIB) $(bindir)
$(bindir)/ThiefSelectors.o: ThiefSelectors.cpp ThiefSelectors.h Selectors.h Generator.h Monolog.h AppInterface.h globals.h | $(LGLIB) $(bindir)
$(bindir)/ThiefRandomizer.o: ThiefRandomizer.cpp ThiefRandomizer.h ThiefReplacers.h ThiefSelectors.h Selectors.h Generator.h Monolog.h AppInterface.h globals.h | $(LGLIB) $(bindir)
$(bindir)/Selectors.o: Selectors.cpp Selectors.h Generator.h Monolog.h AppInterface.h globals.h | $(LGLIB) $(bindir)
$(bindir)/Randomizer.o: Randomizer.cpp Randomizer.h ThiefRandomizer.h Selectors.h Generator.h Monolog.h AppInterface.h globals.h | $(LGLIB) $(bindir)
$(bindir)/SingleScriptModule.o: SingleScriptModule.cpp SingleScriptModule.h SingleScript.h AppInterface.h globals.h | $(LGLIB) $(bindir)
$(bindir)/SingleScript.o: SingleScript.cpp SingleScript.h Randomizer.h Generator.h Monolog.h AppInterface.h globals.h | $(LGLIB) $(bindir)
$(bindir)/SingleInstance.o: SingleInstance.cpp SingleScript.h Monolog.h AppInterface.h globals.h | $(LGLIB) $(bindir)
$(bindir)/Monolog.o: Monolog.cpp Monolog.h globals.h | $(LGLIB) $(bindir)
$(bindir)/AppInterface.o: AppInterface.cpp AppInterface.h globals.h | $(LGLIB) $(bindir)
$(bindir)/Allocator.o: Allocator.cpp globals.h | $(LGLIB) $(bindir)
$(bindir)/globals.o: globals.cpp globals.h | $(LGLIB) $(bindir)
