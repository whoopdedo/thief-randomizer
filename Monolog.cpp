/******************************************************************************
 *  Monolog.cpp
 *
 *  This file is part of Thief Randomizer
 *  Copyright (C) 2022 Tom N Harris <telliamed@whoopdedo.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/
#include "globals.h"
#include "Monolog.h"

#define Nil cScrStr::Null

cAnsiStr cMonolog::ObjectString(int iObjId) const
{
	if (!iObjId)
		return "None (0)";

	cAnsiStr str;
	const char* pszName = OS->GetName(iObjId);
	if (!pszName)
	{
		int iArchId = TM->GetArchetype(iObjId);
		while (!pszName && iArchId)
		{
			pszName = OS->GetName(iArchId);
			iArchId = TM->GetArchetype(iArchId);
		}
		if (pszName)
			switch (pszName[0])
			{
			  case 'A': case 'a': case 'E': case 'e': case 'I': case 'i':
			  case 'O': case 'o': case 'U': case 'u':
				str.FmtStr("An %s (%d)", pszName, iObjId);
				break;
			  default:
				str.FmtStr("A %s (%d)", pszName, iObjId);
				break;
			}
		else
			str.FmtStr("UNKNOWN (%d)", iObjId);
	}
	else
	{
		str.FmtStr("%s (%d)", pszName, iObjId);
	}
	return str;
}

void cMonolog::Print(const cScrStr& sMessage) const
{
	DS->MPrint(m_sName, sMessage, Nil, Nil, Nil, Nil, Nil, Nil);
}

void cMonolog::Print(const cScrStr& sMessage, const object& oArg) const
{
	DS->MPrint(m_sName, sMessage, " ", ObjectString(oArg), Nil, Nil, Nil, Nil);
}

void cMonolog::Log(const cScrStr& sMessage) const
{
	DS->Log(m_sName, sMessage, Nil, Nil, Nil, Nil, Nil, Nil);
}

void cMonolog::Log(const cScrStr& sMessage, const object& oArg) const
{
	cAnsiStr sObject = ObjectString(oArg);
	DS->Log(m_sName, sMessage, " ", sObject, Nil, Nil, Nil, Nil);
}

#ifdef PLAYTEST
void cMonolog::Debug(const cScrStr& sMessage) const
{
	DS->MPrint(m_sName, sMessage, Nil, Nil, Nil, Nil, Nil, Nil);
	DS->Log(m_sName, sMessage, Nil, Nil, Nil, Nil, Nil, Nil);
}

void cMonolog::Debug(const cScrStr& sMessage, const object& oArg) const
{
	cAnsiStr sObject = ObjectString(oArg);
	DS->MPrint(m_sName, sMessage, " ", sObject, Nil, Nil, Nil, Nil);
	DS->Log(m_sName, sMessage, " ", sObject, Nil, Nil, Nil, Nil);
}
#endif

cMonolog::cMonolog(const char* pszName, int iObjId)
{
	if (iObjId)
	{
		m_sName.FmtStr("%s[%d]: ", pszName, iObjId);
	}
	else
	{
		m_sName.FmtStr("%s: ", pszName);
	}
}

