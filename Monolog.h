/******************************************************************************
 *  Monolog.h
 *
 *  This file is part of Thief Randomizer
 *  Copyright (C) 2022 Tom N Harris <telliamed@whoopdedo.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/
#include <lg/objects.h>
#include <lg/scrservices.h>

class cMonolog
{
public:
	void Print(const cScrStr& sMessage) const;
	void Print(const cScrStr& sMessage, const object& oArg) const;
	void Log(const cScrStr& sMessage) const;
	void Log(const cScrStr& sMessage, const object& oArg) const;

#ifdef PLAYTEST
	void Debug(const cScrStr& sMessage) const;
	void Debug(const cScrStr& sMessage, const object& oArg) const;
#else
	void Debug(const cScrStr& sMessage) const
		{ }
	void Debug(const cScrStr& sMessage, const object& oArg) const
		{ }
#endif

	cMonolog(const char* pszName, int iObjId=0);

private:
	cAnsiStr m_sName;
	AppService<IDebugScrSrv> DS;
	AppInterface<IObjectSystem> OS;
	AppInterface<ITraitManager> TM;

	cAnsiStr ObjectString(int iObjId) const;
};
