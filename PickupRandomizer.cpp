/******************************************************************************
 *  PickupRandomizer.cpp
 *
 *  This file is part of Thief Randomizer
 *  Copyright (C) 2022 Tom N Harris <telliamed@whoopdedo.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/
#include "globals.h"
#include <lg/interface.h>
#include <lg/objects.h>
#include <lg/properties.h>
#include <lg/links.h>
#include <lg/propdefs.h>
#include "Generator.h"
#include "Monolog.h"
#include "Randomizer.h"
#include "ThiefRandomizer.h"

cPickupRandomizer::cPickupRandomizer()
{
}

void cPickupRandomizer::AddClass(int iArchetypeId)
{
	m_classObjList.push_back(iArchetypeId);
}

void cPickupRandomizer::AddObject(int iObjId)
{
	m_objList.push_back(iObjId);
}

void cPickupRandomizer::Randomize(randomizer_t& rng, std::function<int(int,int)> cbReplace)
{
	rng.shuffle(m_objList.begin(), m_objList.end());
	for (auto iObjId : m_objList)
	{
		cbReplace(iObjId, rng.range(m_classObjList.begin(), m_classObjList.end()));
	}
}
