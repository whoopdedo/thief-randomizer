/******************************************************************************
 *  Randomizer.cpp
 *
 *  This file is part of Thief Randomizer
 *  Copyright (C) 2022 Tom N Harris <telliamed@whoopdedo.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/
#include "globals.h"
#include <lg/interface.h>
#include <lg/objects.h>
#include <lg/properties.h>
#include "Monolog.h"
#include "Generator.h"
#include "Randomizer.h"
#include "ThiefRandomizer.h"

using namespace std;

cRandomizer::~cRandomizer()
{
	delete m_pRandomizer;
}

cRandomizer::cRandomizer(const char* pszGame)
	: Mono("Randomizer")
{
	m_pRandomizer = new cThiefRandomizer();

	Mono.Log("This is Thief Randomizer, Copyright (C) 2022 Tom N Harris");
}

void cRandomizer::Run(const char* pszGam, const char* pszMis)
{
	cAnsiStr debugMsg;
	debugMsg.FmtStr("Detected mission %s using Gamsys %s.", pszMis, pszGam);
	Mono.Log(debugMsg);

	/*
	 * Setup definitions of classes that objects will be grouped into.
	 */
	Mono.Log("Creating classes.");
	m_pRandomizer->CreateClasses();

	/*
	 * Organize the objects into their respective classes.
	 */
	Mono.Log("Adding objects.");
	m_pRandomizer->AddObjects();

	/*
	 * Shuffle the lists and modify the objects.
	 */
	random_seed_t seed = random_default_seed;
	randomizer_t rng(seed);

	Mono.Log("Randomizing.");
	m_pRandomizer->Randomize(rng);

}

