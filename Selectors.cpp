/******************************************************************************
 *  Selectors.cpp
 *
 *  This file is part of Thief Randomizer
 *  Copyright (C) 2022 Tom N Harris <telliamed@whoopdedo.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/
#include "globals.h"
#include <lg/interface.h>
#include <lg/scrmanagers.h>
#include "Monolog.h"
#include "Selectors.h"
#include <algorithm>
#include <functional>
#include <set>

unsigned int cSelector::ObjectHeight(int iObjId)
{
	int height = 0;
	while (iObjId != -1)
	{
		int iArch = TM->GetArchetype(iObjId);
		if (!iArch || iArch == iObjId)
			break;
		height++;
		iObjId = iArch;
	}
	return height;
}

cSelector::cSelector()
	: Mono("Object Selector")
{
}

void cSelector::AddClass(int iArchetypeId, bool bInvert, bool bOnlyOne, int iWeight)
{
	ClassEntry new_entry = {(unsigned short)iWeight, !bInvert, bOnlyOne};
	ClassKey new_key = {ObjectHeight(iArchetypeId), iArchetypeId};
	m_ClassList.insert_or_assign(new_key, new_entry);
}

bool cSelector::WantsObject(int iObjId) const
{
	for (const auto& found : m_ClassList)
	{
		if (found.first.id == iObjId
		|| (found.second.single ?
		    TM->ObjHasDonorIntrinsically(iObjId, found.first.id)
		  : TM->ObjHasDonor(iObjId, found.first.id)))
		{
			return found.second.select;
		}
	}
	return false;
}

cObjSelector::cObjSelector()
	: cSelector("Object Selector"), PM(),
	  ModelName(PM->GetPropertyNamed("ModelName"))
{
}

cObjSelector::cObjSelector(const char* pszClassName)
	: cSelector(pszClassName), PM(),
	  ModelName(PM->GetPropertyNamed("ModelName"))
{
}

void cObjSelector::AddClass(int iArchetypeId, bool bInvert, bool bOnlyOne, int iWeight)
{
	ClassEntry new_entry = {(unsigned short)iWeight, !bInvert, bOnlyOne};
	ClassKey new_key = {ObjectHeight(iArchetypeId), iArchetypeId};
	m_ClassList.insert_or_assign(new_key, new_entry);
	m_ClassObjList.insert_or_assign(iArchetypeId, new_entry);
}

void cObjSelector::GetClasseObjects(std::function<void(int)> cbObject) const
{
	std::set<int> vMPs;

	std::function<void(int)> AddChildren;
	AddChildren = [this,&cbObject,&vMPs,&AddChildren](int iObj)
	{
		for(SInterface<IObjectQuery> child = TM->Query(iObj, kTraitQueryChildren);
		    !child->Done(); child->Next())
		{
			int iChild = child->Object();
			if (iChild > 0)
				continue;
			if (std::any_of(vMPs.cbegin(), vMPs.cend(),
			   [this,iChild](int mp)->bool { return TM->ObjHasDonor(iChild, mp); }))
				continue;
			auto knownObj = m_ClassObjList.find(iChild);
			if (knownObj == m_ClassObjList.end())
			{
				// Object not in our class list, treat as normal child.
				if (WantsClass(iChild))
					cbObject(iChild);
				AddChildren(iChild);
			}
			else
			{
				// If the object is known it will be iterated on another pass.
				// But descend past if it's a solo-no-select
				if (!knownObj->second.select && knownObj->second.single)
					AddChildren(iChild);
			}
		}
	};

	for (auto iter = m_ClassList.crbegin(); iter != m_ClassList.crend(); ++iter)
	{
		if (iter->second.select)
		{
			int iArchetypeId = iter->first.id;
			if (WantsClass(iArchetypeId))
				cbObject(iArchetypeId);
			if (!iter->second.single)
				AddChildren(iArchetypeId);
			if (TM->IsMetaProperty(iArchetypeId))
				vMPs.insert(iArchetypeId);
		}
	}
}

bool cObjSelector::WantsClass(int iObjId) const
{
	return ModelName->IsRelevant(iObjId);
}

bool cObjSelector::WantsObject(int iObjId) const
{
	if (m_ClassObjList.find(iObjId) != m_ClassObjList.end())
	{
		return m_ClassObjList.at(iObjId).select;
	}
	return cSelector::WantsObject(iObjId);
}
