/******************************************************************************
 *  Selectors.h
 *
 *  This file is part of Thief Randomizer
 *  Copyright (C) 2022 Tom N Harris <telliamed@whoopdedo.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/
#include <lg/scrmanagers.h>
#include <lg/properties.h>
#include <map>
#include <vector>

class cSelector
{
public:
	void AddClass(int iArchetypeId, bool bInvert=false, bool bOnlyOne=false, int iWeight=1);
	bool WantsObject(int iObjId) const;

	cSelector();

protected:
	struct ClassKey
	{
		unsigned int height;
		int id;

		// id compared in reverse to catch concrete objects first
		operator< (const ClassKey& _rhs) const
			{ return height==_rhs.height ? id > _rhs.id : height > _rhs.height; }
	};

	struct ClassEntry
	{
		unsigned short weight;
		unsigned char select;
		unsigned char single;
	};

	std::map<ClassKey,ClassEntry> m_ClassList;

	unsigned int ObjectHeight(int iObjId);

	cSelector(const char* pszClassName)
		: Mono(pszClassName) { }

	cMonolog Mono;
	AppInterface<ITraitManager> TM;
	AppInterface<IObjectSystem> OS;
};

class cObjSelector : public cSelector
{
public:
	void AddClass(int iArchetypeId, bool bInvert=false, bool bOnlyOne=false, int iWeight=1);
	void GetClasseObjects(std::function<void(int)> cbObject) const;
	bool WantsClass(int iObjId) const;
	bool WantsObject(int iObjId) const;

	cObjSelector();

protected:
	std::map<int,ClassEntry> m_ClassObjList;

	cObjSelector(const char* pszClassName);

	AppInterface<IPropertyManager> PM;
	SInterface<IProperty> ModelName;
};
