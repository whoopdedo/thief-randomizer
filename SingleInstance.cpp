/******************************************************************************
 *  SingleInstance.cpp
 *
 *  This file is part of Thief Randomizer
 *  Copyright (C) 2022 Tom N Harris <telliamed@whoopdedo.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/
#include "globals.h"
#include "Monolog.h"
#include "SingleScript.h"
#include <lg/interface.h>
#include <lg/objects.h>
#include <lg/properties.h>
#include <lg/propdefs.h>
#include <lg/scrservices.h>
#include <cstring>
#include <cctype>

static size_t strtolower(char *dst, char const *src, size_t max_n)
{
	size_t n = 0;
	for (;*src && n<=max_n; ++n)
		*dst++ = ::tolower(*src++);
	return n;
}

static void remove_ext(char *str, char const *ext)
{
	size_t len = ::strlen(str), xlen = ::strlen(ext);
	char *strext = str + (len-xlen);
	if (::stricmp(strext, ext) == 0)
		*strext = '\0';
}


int cSingleScript::sm_iDromed = -1;
char cSingleScript::sm_szGame[16] = "";
char cSingleScript::sm_szGamesys[64] = "";
char cSingleScript::sm_szMission[64] = "";

bool cSingleScript::SingleInstance(const char* pszName, int iObjId)
{
	if (sm_iDromed == 0xFF) // already failed version check
		return false;

	cMonolog Mono("SingleInstance");
	try
	{
		AppService<IVersionSrv> pNewDark;
		// Only need to read the game version and mission name once.
		if (sm_iDromed < 0)
		{
			int major, minor;
			pNewDark->GetVersion(major, minor);
			// Require at least Thief 1.23 or SS2 2.44
			if (!( (major == 1 && minor >= 23) || (major == 2 && minor >= 44) ))
			{
				sm_iDromed = 0xFF;
				return false;
			}
			cAutoStr strGame(1), strGamesys(1), strMission(1);
			pNewDark->GetGame(strGame);
			strtolower(sm_szGame, strGame, 15);
			pNewDark->GetGamsys(strGamesys);
			strtolower(sm_szGamesys, strGamesys, 63);
			remove_ext(sm_szGamesys, ".gam");
			pNewDark->GetMap(strMission);
			strtolower(sm_szMission, strMission, 63);
			remove_ext(sm_szMission, ".mis");
		}
		// Don't do anything in editor mode,
		sm_iDromed = pNewDark->IsEditor();
#ifndef PLAYTEST
		if (sm_iDromed == 1)
			return false;
#endif
	}
	catch (no_interface&)
	{
		// Must be MPrint since Log probably isn't available.
		Mono.Print("Can't get game version. Script won't run.");
		sm_iDromed = 0xFF;
		return false;
	}

	sScrDatumTag datum = {0, pszName, "singlescript_host_id"};
	if (g_pScriptManager->IsScriptDataSet(&datum))
	{
		cMultiParm mpHostId;
		g_pScriptManager->GetScriptData(&datum, &mpHostId);
		if (mpHostId == iObjId)
		{
			Mono.Print("Starting script on ", iObjId);
			return true;
		}
		return false;
	}

	AppInterface<IObjectSystem> pOS;
	AppInterface<IPropertyManager> pPM;
	auto pScriptProp = interface_cast<IScriptProperty>(pPM->GetPropertyNamed("Scripts"));
	auto pDiffProp = interface_cast<IGenericProperty>(pPM->GetPropertyNamed("DiffScript"));

	// If the script is already instantiated on a concrete object, use that instead
	// of making a new singleton.
	if (pScriptProp->IsSimplyRelevant(iObjId))
	{
		sScriptProp* scrProp;
		pScriptProp->Get(iObjId, &scrProp);
		for (int n=0; n<4; n++)
			if (::stricmp(scrProp->script[n], pszName) == 0)
			{
				cMultiParm mpHostId = iObjId;
				g_pScriptManager->SetScriptData(&datum, &mpHostId);
				if (pDiffProp->Create(iObjId) == S_OK)
				{
					sDatum diff;
					pDiffProp->Get(iObjId, &diff.pv);
					diff.i |= 0xF;
					pDiffProp->Set(iObjId, diff.pv);
				}
				Mono.Print("Starting script on ", iObjId);
				return true;
			}
	}

	// If the randomizer script is on the same archetype it will cause an infinite loop.
	// Setting the script data to a temporary value will stop that.
	cMultiParm mpHostId = -1;
	g_pScriptManager->SetScriptData(&datum, &mpHostId);
	int iHostObj = pOS->BeginCreate(pOS->GetObjectNamed("Marker"), kObjQueryConcrete);
	if (iHostObj)
	{
		Mono.Print("Creating singleton object ", object(iHostObj));

		mpHostId = iHostObj;
		g_pScriptManager->SetScriptData(&datum, &mpHostId);

		sScriptProp scrProp = { {"","","",""}, TRUE };
		sDatum diff = { 0xF };
		pOS->SetObjTransience(iHostObj, TRUE);
		pDiffProp->Create(iHostObj);
		pDiffProp->Set(iHostObj, diff.pv);
		pScriptProp->Create(iHostObj);
		::strncpy(scrProp.script[0], pszName, sizeof(scrProp.script[0])-1);
		pScriptProp->Set(iHostObj, &scrProp);
		pOS->EndCreate(iHostObj);
	}

	return false;
}
