/******************************************************************************
 *  SingleScript.cpp
 *
 *  This file is part of Thief Randomizer
 *  Copyright (C) 2022 Tom N Harris <telliamed@whoopdedo.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/
#include "globals.h"
#include "Generator.h"
#include "Monolog.h"
#include "Randomizer.h"
#include "SingleScript.h"
#include <lg/interface.h>
#include <lg/objects.h>
#include <lg/scrmsgs.h>
#include <cstring>
#include <exception>

STDMETHODIMP_(const char*) cSingleScript::GetClassName(void)
{
	return g_cszScriptName;
}

STDMETHODIMP cSingleScript::ReceiveMessage(sScrMsg* pMsg, sMultiParm* pReply, eScrTraceAction eTrace)
{
	if (!::_stricmp(pMsg->message, "Difficulty"))
	{
		try
		{
			cRandomizer tricksie(sm_szGame);
			tricksie.Run(sm_szGamesys, sm_szMission);
		}
		catch (std::exception& err)
		{
			g_pfnMPrintf("Exception occurred in Run(): %s", err.what());
		}
		catch (...)
		{
			g_pfnMPrintf("Unknown exception occured in Run().");
		}
	}
	else if (!::_stricmp(pMsg->message, "Sim"))
	{
		if (static_cast<sSimMsg*>(pMsg)->fStarting)
		{
			//AppInterface<IObjectSystem>()->Destroy(m_iObjId);
		}
	}
	return 0;
}

cSingleScript::~cSingleScript()
{
}

cSingleScript::cSingleScript(int iHostObjId)
	: m_iObjId(iHostObjId)
{
}

IScript* __cdecl cSingleScript::ScriptFactory(const char* pszName, int iHostObjId)
{
	if (::_stricmp(pszName, g_cszScriptName) != 0)
		return NULL;
	if (!SingleInstance(pszName, iHostObjId))
		return NULL;
	return new(std::nothrow) cSingleScript(iHostObjId);
}
