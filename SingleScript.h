/******************************************************************************
 *  SingleScript.h
 *
 *  This file is part of Thief Randomizer
 *  Copyright (C) 2022 Tom N Harris <telliamed@whoopdedo.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/
#include <lg/interfaceimp.h>
#include <lg/script.h>

class cSingleScript : public cInterfaceImp<IScript>
{
	STDMETHOD_(const char*,GetClassName)(void);
	STDMETHOD(ReceiveMessage)(sScrMsg* pMsg, sMultiParm* pReply, eScrTraceAction eTrace);

private:
	int m_iObjId;

	static bool SingleInstance(const char* pszName, int iHostObjId);

	static int sm_iDromed;
	static char sm_szGame[16];
	static char sm_szGamesys[64];
	static char sm_szMission[64];

public:
	virtual ~cSingleScript();
	cSingleScript(int iHostObjId);

	static IScript* __cdecl ScriptFactory(const char* pszName, int iHostObjId);
};
