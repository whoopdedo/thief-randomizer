/******************************************************************************
 *  SingleScriptModule.cpp
 *
 *  This file is part of Thief Randomizer
 *  Copyright (C) 2022 Tom N Harris <telliamed@whoopdedo.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/
#include "globals.h"
#include "Monolog.h"
#include "SingleScriptModule.h"
#include "SingleScript.h"
#include <cstring>

cSingleScriptModule  g_ScriptModule;

STDMETHODIMP_(const char*) cSingleScriptModule::GetName(void)
{
	return m_szName;
}

STDMETHODIMP_(const sScrClassDesc*) cSingleScriptModule::GetFirstClass(tScrIter* pIterParam)
{
	return &sm_ScriptDesc;
}

STDMETHODIMP_(const sScrClassDesc*) cSingleScriptModule::GetNextClass(tScrIter* pIterParam)
{
	return NULL;
}
STDMETHODIMP_(void) cSingleScriptModule::EndClassIter(tScrIter* pIterParam)
{
}

cSingleScriptModule::~cSingleScriptModule()
{
}
cSingleScriptModule::cSingleScriptModule()
{
	SetName(g_cszScriptName);
}

void cSingleScriptModule::SetName(const char* pszName)
{
	if (pszName)
	{
		::strncpy(m_szName, pszName, sizeof(m_szName));
		m_szName[sizeof(m_szName)-1] = 0;
	}
	else
		SetName(g_cszScriptName);
}

const sScrClassDesc cSingleScriptModule::sm_ScriptDesc = {
	g_cszModuleName,
	g_cszScriptName,
	"Script",
	cSingleScript::ScriptFactory
};

void cSingleScriptModule::DisposeRef(void)
{
	AppInterface<IScriptMan>::Release();
}

extern "C"
int __declspec(dllexport) __stdcall
ScriptModuleInit (const char* pszName,
                  IScriptMan* pScriptMan,
                  MPrintfProc pfnMPrintf,
                  IMalloc* pMalloc,
                  IScriptModule** pOutInterface)
{
	*pOutInterface = NULL;

	g_pScriptManager = pScriptMan;
	g_pMalloc = pMalloc;

	if (pfnMPrintf)
		g_pfnMPrintf = pfnMPrintf;

	if (!g_pScriptManager || !g_pMalloc)
		return 0;

	AppInterface<IScriptMan>::Init(g_pScriptManager);

	g_ScriptModule.SetName(pszName);
	g_ScriptModule.QueryInterface(IID_IScriptModule, reinterpret_cast<void**>(pOutInterface));

	return 1;
}
