/******************************************************************************
 *  SingleScriptModule.h
 *
 *  This file is part of Thief Randomizer
 *  Copyright (C) 2022 Tom N Harris <telliamed@whoopdedo.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/
#include <lg/interfaceimp.h>
#include <lg/script.h>

class cSingleScriptModule : public cInterfaceImp<IScriptModule,IID_Def<IScriptModule>,kInterfaceImpStatic>
{
public:
	// IUnknown
	//STDMETHOD(QueryInterface)(REFIID,void**);
	//STDMETHOD_(ULONG,AddRef)(void);
	//STDMETHOD_(ULONG,Release)(void);
	// IScriptModule
	STDMETHOD_(const char*,GetName)(void);
	STDMETHOD_(const sScrClassDesc*,GetFirstClass)(tScrIter* pIterParam);
	STDMETHOD_(const sScrClassDesc*,GetNextClass)(tScrIter* pIterParam);
	STDMETHOD_(void,EndClassIter)(tScrIter* pIterParam);

	virtual ~cSingleScriptModule();
	cSingleScriptModule();

	void SetName(const char* pszName);

private:
	char m_szName[64];
	static const sScrClassDesc sm_ScriptDesc;

protected:
	virtual void DisposeRef(void);
};

