/******************************************************************************
 *  ThiefRandomizer.cpp
 *
 *  This file is part of Thief Randomizer
 *  Copyright (C) 2022 Tom N Harris <telliamed@whoopdedo.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/
#include "globals.h"
#include <lg/interface.h>
#include <lg/objects.h>
#include <lg/properties.h>
#include <lg/links.h>
#include <lg/propdefs.h>
#include "Generator.h"
#include "Monolog.h"
#include "Randomizer.h"
#include "ThiefRandomizer.h"

cThiefRandomizer::cThiefRandomizer()
	: Mono("Thief Randomizer")
{
}

void cThiefRandomizer::CreatureClasses(void)
{
	int iObj = OS->GetObjectNamed("Creature");
	if (iObj)
	{
		m_AISelector.AddClass(iObj);
		for (auto pszName : {"servant",
		                     "Actor",
				     "OperaGuest",
				     "UThief",
				     "Grunts",
				     "Cops",
				     "Sheriff",
				     "AppleBowman",
				     "hammerite",
				     "Mage",
				     "Spider",
				     "ZombieTypes",
				     "Trickster",
				     "Murus",
				     "ClockworkEye",
				     "MechCherub"})
		{
			iObj = OS->GetObjectNamed(pszName);
			if (iObj)
				m_AISelector.AddClass(iObj, true, true);
		}
		for (auto pszName : {"HammerCorpse",
		                     "Mech_Corpse",
				     "Defective Burrick",
				     "DeadBurrick1",
				     "DeadBurrick2",
				     "Phantom",
				     "Cameras",
				     "Turret",
				     "SmallRobot"})
		{
			iObj = OS->GetObjectNamed(pszName);
			if (iObj)
				m_AISelector.AddClass(iObj, true);
		}
	}
}

void cThiefRandomizer::LootClasses(void)
{
	for (auto pszName : {"IsLoot",
	                     "Food",
			     "Dish",
			     "Musical",
			     "Hammers",
			     "Projectile",
			     "Crystal",
			     "Grenadz",
			     "Potions",
			     "Flare",
			     "QuestItems",
			     "Equipment",
			     "Potted1",
			     "QuestyGear"})
	{
		int iObj = OS->GetObjectNamed(pszName);
		if (iObj)
			m_LootSelector.AddClass(iObj);
	}
	for (auto pszName : {"firearr",
	                     "AssassinArrow",
			     "TurretArrow",
			     "QuivArrow",
			     "XBowDart",
			     "CritterShots",
			     "EarthArrow",
			     "GasArrow",
			     "ActiveMine",
			     "ActiveGasMine",
			     "DischargingMine",
			     "AllTheTalismans",
			     "LC_Masks",
			     "Hagen'sCloth",
			     "HagenEvidence",
			     "SheriffEvidence",
			     "RustGasCan",
			     "Cultivator",
			     "Miss6Stuff",
			     "EndGame",
			     "ThrownApple",
			     "LockPick",
			     "Locks",
			     "Container",
			     "Plaque",
			     "Key",
			     "Key-or-Part",
			     "Compass",
			     "Compass2"})
	{
		int iObj = OS->GetObjectNamed(pszName);
		if (iObj)
			m_LootSelector.AddClass(iObj, true);
	}
	for (auto pszName : {"vase",
	                     "Plate",
			     "Bottle",
			     "Goblet",
			     "TrGob"})
	{
		int iObj = OS->GetObjectNamed(pszName);
		if (iObj)
			m_LootSelector.AddClass(iObj, true, true);
	}
}

void cThiefRandomizer::StartingPoint(void)
{
	auto pScriptProp = interface_cast<IScriptProperty>(PM->GetPropertyNamed("Scripts"));
	sPropertyObjIter iter;
	pScriptProp->IterStart(&iter);
	int iScrObj = 0;
	sScriptProp* scrProp;
	while (pScriptProp->IterNextValue(&iter, &iScrObj, &scrProp))
		if (iScrObj > 0)
		{
			for (const auto& szScript : scrProp->script)
			{
				if (!stricmp(szScript, "VictoryCheck")
				 || !stricmp(szScript, "ListenClosely") // Eavesdropping name for VictoryCheck
				   )
				{
					Mono.Debug("Found StartingPoint ", iScrObj);
					ExcludeLinks(iScrObj, "Contains");
					break;
				}
				else if (!stricmp(szScript, "LoadoutCache"))
				{
					Mono.Debug("Found LoadoutCache ", iScrObj);
					ExcludeLinks(iScrObj, "Contains");
					break;
				}
				else if (!stricmp(szScript, "LastMissionLoot")
				      || !stricmp(szScript, "MagicBones")
				      || !stricmp(szScript, "MrCavador"))
				{
					Mono.Debug("Skipping object because of script ", iScrObj);
					ExcludeObject(iScrObj);
					break;
				}
			}
		}
	pScriptProp->IterStop(&iter);
}

void cThiefRandomizer::Store(void)
{
	auto pStoreProp = interface_cast<IBoolProperty>(PM->GetPropertyNamed("ItemStore"));
	sPropertyObjIter iter;
	pStoreProp->IterStart(&iter);
	int iObj = 0;
	Bool isStore = false;
	while (pStoreProp->IterNextValue(&iter, &iObj, &isStore))
		if (iObj > 0 && isStore)
		{
			Mono.Debug("Found Store ", iObj);
			ExcludeLinks(iObj, "Contains");
		}
	pStoreProp->IterStop(&iter);
}

void cThiefRandomizer::ExcludeLinks(int iObjId, const char* pszLinkKind)
{
	auto pLink = interface_cast<IRelation>(LM->GetRelationNamed(pszLinkKind));
	SInterface<ILinkQuery> pQuery = pLink->Query(iObjId, 0);
	for (; !pQuery->Done(); pQuery->Next())
	{
		sLink link;
		pQuery->Link(&link);
		Mono.Debug(pszLinkKind, link.dest);
		m_RejectingSelector.AddClass(link.dest);
	}
}

void cThiefRandomizer::ExcludeObject(int iObjId)
{
	m_RejectingSelector.AddClass(iObjId);
}

void cThiefRandomizer::CreateClasses(void)
{
	CreatureClasses();
	LootClasses();
	StartingPoint();
	Store();

	for (auto pszName : {"VictoryLoot", "Farkus", "Ramirez", "Basso", "Jenivere"})
	{
		int iObj = OS->GetObjectNamed(pszName);
		if (iObj)
			ExcludeObject(iObj);
	}
}

void cThiefRandomizer::AddObjects(void)
{
	m_AISelector.GetClasses([this](int iArchetypeId) {
		m_AIRandomizer.AddClass(iArchetypeId);
	});
	m_AISelector.GetObjects([this](int iObjId) {
		if (!m_RejectingSelector.WantsObject(iObjId))
			m_AIRandomizer.AddObject(iObjId);
	});
	m_LootSelector.GetObjects([this](int iObjId) {
		if (!m_RejectingSelector.WantsObject(iObjId))
			m_LootRandomizer.AddObject(iObjId);
	});
}

void cThiefRandomizer::Randomize(randomizer_t& rng)
{
	m_AIRandomizer.Randomize(rng, [this](int iObjId, int iArchetypeId) {
		return m_AIReplacer.CreateObject(iArchetypeId, iObjId);
	});
	m_LootRandomizer.Randomize(rng, [this](int iObjId, int iArchetypeId) {
		return m_LootReplacer.CreateObject(iArchetypeId, iObjId);
	});
}
