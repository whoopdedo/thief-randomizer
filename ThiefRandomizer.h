/******************************************************************************
 *  ThiefRandomizer.h
 *
 *  This file is part of Thief Randomizer
 *  Copyright (C) 2022 Tom N Harris <telliamed@whoopdedo.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/
#include "ThiefSelectors.h"
#include "ThiefReplacers.h"
#include <vector>
#include <functional>

class cAIRandomizer
{
public:
	cAIRandomizer();

	void AddClass(int iArchetypeId);
	void AddObject(int iObjId);
	void Randomize(randomizer_t& rng, std::function<int(int,int)> cbReplace);

private:
	std::vector<int> m_classObjList;
	std::vector<int> m_objList;
};

class cPickupRandomizer
{
public:
	cPickupRandomizer();

	void AddClass(int iArchetypeId);
	void AddObject(int iObjId);
	void Randomize(randomizer_t& rng, std::function<int(int,int)> cbReplace);

private:
	std::vector<int> m_classObjList;
	std::vector<int> m_objList;
};

class cThiefRandomizer
{
public:
	cThiefRandomizer();

protected:
	cObjSelector m_RejectingSelector;
	cAISelector m_AISelector;
	cPickupSelector m_LootSelector;

	cAIRandomizer m_AIRandomizer;
	cPickupRandomizer m_LootRandomizer;

	cAIReplacer m_AIReplacer;
	cObjReplacer m_LootReplacer;

	void CreatureClasses(void);
	void LootClasses(void);
	void StartingPoint(void);
	void Store(void);

	void ExcludeObject(int iObjId);
	void ExcludeLinks(int iObjId, const char* pszLinkKind);

	void CreateClasses(void);
	void AddObjects(void);
	void Randomize(randomizer_t& rng);

	friend class cRandomizer;

private:
	AppInterface<IObjectSystem> OS;
	AppInterface<IPropertyManager> PM;
	AppInterface<ILinkManager> LM;
	cMonolog Mono;
};
