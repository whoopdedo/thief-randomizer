/******************************************************************************
 *  ThiefReplacers.cpp
 *
 *  This file is part of Thief Randomizer
 *  Copyright (C) 2022 Tom N Harris <telliamed@whoopdedo.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/
#include "globals.h"
#include <lg/interface.h>
#include <lg/ai.h>
#include <lg/objects.h>
#include <lg/properties.h>
#include <lg/scrmanagers.h>
#include <lg/scrservices.h>
#include <lg/tools.h>
#include <lg/defs.h>
#include <lg/propdefs.h>
#include "Monolog.h"
#include "ThiefReplacers.h"

static const mxs_vector _mxs_vector{0,0,0};
constexpr mxs_vec_zero = &_mxs_vector;
static const mxs_angvec _mxs_angvec{0,0,0};
constexpr mxs_angvec_zero = _mxs_angvec;

cObjReplacer::cObjReplacer()
{
}

int cObjReplacer::CreateObject(int iArchetypeId, int iReferenceObj)
{
	int iObjId = OS->BeginCreate(iArchetypeId, kObjQueryConcrete);
	if (iReferenceObj)
	{
		GT->TeleportObject(iObjId, mxs_vec_zero, mxs_angvec_zero, iReferenceObj);
	}
	OS->EndCreate(iObjId);
	return iObjId;
}

cAIReplacer::cAIReplacer()
{
}

int cAIReplacer::CreateObject(int iArchetypeId, int iReferenceObj)
{
	return cObjReplacer::CreateObject(iArchetypeId, iReferenceObj);
}
