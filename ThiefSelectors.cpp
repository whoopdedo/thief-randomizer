/******************************************************************************
 *  ThiefSelectors.cpp
 *
 *  This file is part of Thief Randomizer
 *  Copyright (C) 2022 Tom N Harris <telliamed@whoopdedo.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/
#include "globals.h"
#include <lg/interface.h>
#include <lg/ai.h>
#include <lg/objects.h>
#include <lg/properties.h>
#include <lg/scrmanagers.h>
#include <lg/scrservices.h>
#include <lg/defs.h>
#include <lg/propdefs.h>
#include "Generator.h"
#include "Monolog.h"
#include "ThiefSelectors.h"
#include <algorithm>
#include <set>

cAISelector::cAISelector()
	: cObjSelector("AI Selector"),
	  AIProp(static_cast<IGenericProperty*>(PM->GetPropertyNamed("AI"))),
	  AIModeProp(static_cast<IGenericProperty*>(PM->GetPropertyNamed("AI_Mode"))),
	  MPosedCorpse(OS->GetObjectNamed("M-PosedCorpse"))
{
}

void cAISelector::AddClass(int iArchetypeId, bool bInvert, bool bOnlyOne, int iWeight)
{
	cObjSelector::AddClass(iArchetypeId, bInvert, bOnlyOne, iWeight);
}

void cAISelector::GetObjects(std::function<void(int)> cbObject)
{
	tAIIter iter;
	SInterface<IAI> pAI = AIM->GetFirst(&iter);
	for (; !!pAI; pAI.reset(AIM->GetNext(&iter)))
	{
		int iObjId = pAI->GetID();
		if (WantsObject(iObjId))
		{
			Mono.Debug("Selecting object ", object(iObjId));
			cbObject(iObjId);
		}
	}
	AIM->GetDone(&iter);
}

bool cAISelector::WantsObject(int iObjId) const
{
	// TODO Most of these can be in cAIRandomizer.
	// Behaviorset at least should be there.
	sAIProp* ai;
	sDatum mode;
	if (!AIProp->GetSimple(iObjId, (void**)&ai) || !stricmp(ai->behavior_set, "null"))
		return false;
	if (!AIModeProp->GetSimple(iObjId, &mode.pv) || mode.i == kAIM_Dead)
		return false;
	if (MPosedCorpse && TM->ObjHasDonor(iObjId, MPosedCorpse))
		return false;
	if (MPrisoner && TM->ObjHasDonor(iObjId, MPrisoner))
		return false;
	return cObjSelector::WantsObject(iObjId);
}

cPickupSelector::cPickupSelector()
	: cObjSelector("Loot Selector"),
	  FrobInfo(static_cast<IGenericProperty*>(PM->GetPropertyNamed("FrobInfo"))),
	  LootInfo(static_cast<IGenericProperty*>(PM->GetPropertyNamed("Loot")))
{
}

void cPickupSelector::AddClass(int iArchetypeId, bool bInvert, bool bOnlyOne, int iWeight)
{
	cObjSelector::AddClass(iArchetypeId, bInvert, bOnlyOne, iWeight);
}

void cPickupSelector::GetObjects(std::function<void(int)> cbObject)
{
	SInterface<ITrait> pDonor = FrobInfo;
	std::set<int> visited;
	std::set<int> metaprop;
	for (auto iterClass = m_ClassList.crbegin(); iterClass != m_ClassList.crend(); ++iterClass)
	{
		if (!(iterClass->second.select || iterClass->second.single))
			continue;
		int iArch = iterClass->first.id;
		// Already seen an ancestor so skip this sub-tree
		if (std::any_of(visited.cbegin(), visited.cend(),
		   [this,iArch](int id)->bool { return TM->ObjHasDonor(iArch, id); }))
			continue;
		visited.insert(iArch);
		if (!pDonor->PossessedBy(iArch))
		{
			// I honestly don't know what to do here.
			continue;
		}
		SInterface<IObjectQuery> pQuery = pDonor->GetAllHeirs(iArch, kObjQueryConcrete);
		for (; !pQuery->Done(); pQuery->Next())
		{
			int iObj = pQuery->Object();
			// Don't rescan if we've been to this object via metaproperty
			if (std::any_of(metaprop.cbegin(), metaprop.cend(),
			   [this,iObj](int mp)->bool { return TM->ObjHasDonor(iObj, mp); }))
				continue;
			if (WantsObject(iObj))
			{
				Mono.Debug("Selecting object ", object(iObj));
				cbObject(iObj);
			}
		}
		if (TM->IsMetaProperty(iArch))
			metaprop.insert(iArch);
	}
}

bool cPickupSelector::WantsObject(int iObjId) const
{
	sFrobInfo* pFrobInfo;
	if (!FrobInfo->Get(iObjId, (void**)&pFrobInfo)
	 || (pFrobInfo->World & kFrobActMove) == 0)
		return false;
	// TODO Loot should be managed in cPickupRandomizer
	sLoot* pLoot;
	if (LootInfo->Get(iObjId, (void**)&pLoot) && pLoot->iSpecial)
		return false;
	return cObjSelector::WantsObject(iObjId);
}
