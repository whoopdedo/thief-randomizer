/******************************************************************************
 *  ThiefSelectors.h
 *
 *  This file is part of Thief Randomizer
 *  Copyright (C) 2022 Tom N Harris <telliamed@whoopdedo.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/
#include "Selectors.h"
#include <functional>

/* Selects AI.
 */
class cAISelector : public cObjSelector
{
public:
	void AddClass(int iArchetypeId, bool bInvert=false, bool bOnlyOne=false, int iWeight=1);
	bool WantsObject(int iObjId) const;
	void GetObjects(std::function<void(int)> cbObject);

	cAISelector();
private:
	AppInterface<IAIManager> AIM;
	SInterface<IGenericProperty> AIProp;
	SInterface<IGenericProperty> AIModeProp;
	int MPosedCorpse;
	int MPrisoner;
};

/* Selects objects that can be picked up.
 * Such as loot, tools, and small junk.
 */
class cPickupSelector : public cObjSelector
{
public:
	void AddClass(int iArchetypeId, bool bInvert=false, bool bOnlyOne=false, int iWeight=1);
	bool WantsObject(int iObjId) const;
	void GetObjects(std::function<void(int)> cbObject);

	cPickupSelector();
private:
	SInterface<IGenericProperty> FrobInfo;
	SInterface<IGenericProperty> LootInfo;
};
