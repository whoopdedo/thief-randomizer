/******************************************************************************
 *  globals.cpp
 *
 *  This file is part of Thief Randomizer
 *  Copyright (C) 2022 Tom N Harris <telliamed@whoopdedo.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/
#include "globals.h"

extern "C" DECLSPEC_IMPORT BOOL WINAPI DisableThreadLibraryCalls(HANDLE);

static int __cdecl NullPrintf(const char*, ...)
{
	return 0;
}

IMalloc *g_pMalloc = NULL;
IScriptMan *g_pScriptManager = NULL;
MPrintfProc g_pfnMPrintf = NullPrintf;

extern "C"
BOOL WINAPI
DllMain (HINSTANCE hDLL, DWORD dwReason, PVOID lpResv)
{
	if (dwReason == DLL_PROCESS_ATTACH)
	{
		::DisableThreadLibraryCalls(hDLL);
		return TRUE;
	}
	return TRUE;
}
